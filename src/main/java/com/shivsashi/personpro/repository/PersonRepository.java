package com.shivsashi.personpro.repository;

import com.shivsashi.personpro.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
