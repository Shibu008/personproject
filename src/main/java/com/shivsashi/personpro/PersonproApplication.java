package com.shivsashi.personpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonproApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonproApplication.class, args);
    }

}
