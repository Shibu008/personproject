package com.shivsashi.personpro.controller;

import com.shivsashi.personpro.model.Person;
import com.shivsashi.personpro.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/persons")
    public List<Person> getAllPerson() {
        return personRepository.findAll();
    }

    @PostMapping("/person")
    public Person savePerson(@RequestBody Person person) {
        return personRepository.save(person);
    }

    @GetMapping("/person/{pid}")
    @ResponseBody
    public Person getPerson(@PathVariable("pid") long personId) {
        return personRepository.findById(personId).orElse(new Person());
    }

    @DeleteMapping("/person/{pid}")
    @ResponseBody
    public void deletePerson(@PathVariable("pid") long personId) {
        personRepository.deleteById(personId);
    }

    @PutMapping("/person/{pid}")
    @ResponseBody
    public void updatePerson(@PathVariable("pid") long personId, @RequestBody Person person) {
        Person person1 = personRepository.findById(personId).orElse(new Person());
        person1.setName(person.getName());
        person1.setCountry(person.getCountry());
        personRepository.save(person1);
    }

}
